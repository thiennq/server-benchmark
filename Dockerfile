FROM alpine

# App root
RUN mkdir -p /src
COPY src/files src/files
CMD [ "/bin/sh"]