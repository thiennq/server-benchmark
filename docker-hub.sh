#/bin/bash

# Prepare
mkdir -p temps
mkdir -p src
mkdir -p reports
rm -rf src/files
rm -rf temps/** temps/*.*
IMAGE=alpinelinux/golang:latest
docker rmi $IMAGE -f
docker system prune -a -f 
echo '' > reports/dockerhub.txt;

# Start
ST=$(date '+%s')
docker pull $IMAGE 2>&1 | tee -a reports/dockerhub.txt
ET=$(date '+%s')

echo "From $(date -d @$ST +%H:%M:%S) to $(date -d @$ET +%H:%M:%S)" 2>&1 | tee -a reports/dockerhub.txt
echo "Elapsed Time: $(($ET-$ST)) seconds" 2>&1 | tee -a reports/dockerhub.txt

