const fs = require('fs');
const path = require('path');
const PROJECT_ID = 50459324;
const TOKEN="glpat-Gtdzcwyc3Hu5EcoLkfMK"

const headers = {
    "Content-Type": "application/json",
    "PRIVATE-TOKEN": TOKEN
}
const url = `https://gitlab.com/api/v4/projects/${PROJECT_ID}/snippets`

function upload(title, files) {
    const data = {
        "title": title,
        "visibility" : "public",
        "files": files
    }
    fetch(url, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data),
    })
    .then((response) => response.json())
}

async function main() {
    const filenames = fs.readdirSync(path.join(__dirname, 'reports'));
    const files = filenames.map(f => {
        const content = fs.readFileSync(path.join(__dirname, 'reports', f)).toString('utf-8');
        
        return {
            "file_path": f,
            "content" : f.endsWith('.json') ? JSON.stringify(JSON.parse(content), null, 2) : content
        }
    })
    const ip = await getIp();
    console.log({ip})
    const time = new Date().toISOString();;
    const title = `${ip} - ${time}`;
    await upload(title, files);
}

async function getIp() {
    const url = 'https://api.ipify.org?format=json';
    const data = await fetch(url, {headers: {"Content-Type": "application/json"}, method: "GET"})
        .then(resp => resp.json())
    return data.ip;
}

main().then(() => console.log(`
Upload done!
Read the reports at: https://gitlab.com/thiennq/server-benchmark/-/snippets
`));