#/bin/bash

# Prepare
mkdir -p temps
mkdir -p src
mkdir -p reports
rm -rf src/files
rm -rf temps/** temps/*.*

# Prepare: Docker with gitlab registry
REGISTRY=registry.gitlab.com/thiennq/server-benchmark
USER="deploy"
PASS="2nSjrrADBMZFdXzSiv_f"

echo $PASS | docker login -u "$USER" "$REGISTRY" --password-stdin
dd if=/dev/urandom of=src/files bs=1M count=100

# Test speed upload: Docker with gitlab registry
VERSION=$(date '+%m%d-%H%M')
DEST=$REGISTRY:$VERSION
echo '' > reports/gitlab-registry.txt

# Start
echo "Image will be pushed to $DEST" 2>&1 | tee -a reports/gitlab-registry.txt
ST=$(date '+%s')
docker build . -t $DEST 2>&1 | tee -a reports/gitlab-registry.txt
docker push $DEST 2>&1 | tee -a reports/gitlab-registry.txt
ET=$(date '+%s')

echo "From $(date -d @$ST +%H:%M:%S) to $(date -d @$ET +%H:%M:%S)" 2>&1 | tee -a reports/gitlab-registry.txt
echo "Elapsed Time: $(($ET-$ST)) seconds" 2>&1 | tee -a reports/gitlab-registry.txt
