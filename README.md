# 1. Prerequisite
- Docker
- Node.JS (18 or above): https://nodejs.org/en/download/package-manager#debian-and-ubuntu-based-linux-distributions

# 2. Setup
```bash
git clone https://deploy:2nSjrrADBMZFdXzSiv_f@gitlab.com/thiennq/server-benchmark
cd server-benchmark
```

# 3. Run Test

### 3.1. Full test then send reports to gitlab snippets
```bash
./all.sh
```

### 3.2. Single test
```bash
./clear.sh && ./npm.sh && node snippet.js  # Test npm registry speed
./clear.sh && ./docker-hub.sh && node snippet.js # Test pull image speed from hub.docker.com
./clear.sh && ./gitlab-push.sh && node snippet.js # Test push image to registry.gitlab.com
./clear.sh && ./yabs.sh && node snippet.js # Network speed test by https://yabs.sh
```

### 3.3. Send all current reports to gitlab snippets
```bash
node snippet.js
```

# 4. View reports
- https://gitlab.com/thiennq/server-benchmark/-/snippets