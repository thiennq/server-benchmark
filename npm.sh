#/bin/bash

# Prepare
mkdir -p temps
mkdir -p src
mkdir -p reports
rm -rf src/files
rm -rf temps/** temps/*.*


# Test registry.npmjs.org
ST=$(date '+%s')
wget https://registry.npmjs.org/aws-lambda-libreoffice/-/aws-lambda-libreoffice-0.1.4.tgz -O temps/aws-lambda-libreoffice-0.1.4.tgz --progress=dot:mega 2>&1 | tee reports/npm.txt;
ET=$(date '+%s')

echo "From $(date -d @$ST +%H:%M:%S) to $(date -d @$ET +%H:%M:%S)" 2>&1 | tee -a reports/npm.txt
echo "Elapsed Time: $(($ET-$ST)) seconds" 2>&1 | tee -a reports/npm.txt